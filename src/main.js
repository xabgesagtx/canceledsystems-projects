// This is the main.js file. Import global CSS and scripts here.
// The Client API can be used here. Learn more: gridsome.org/docs/client-api

import '~/assets/style/index.scss'
import DefaultLayout from '~/layouts/Default.vue'
import {library} from '@fortawesome/fontawesome-svg-core'
import {faCodeBranch, faGlobe} from '@fortawesome/free-solid-svg-icons'
import {FontAwesomeIcon} from '@fortawesome/vue-fontawesome'
import BootstrapVue from 'bootstrap-vue'

library.add(faGlobe)
library.add(faCodeBranch)

export default function (Vue, {router, head, isClient}) {
    // Set default layout as a global component
    Vue.use(BootstrapVue)
    Vue.component('fa-icon', FontAwesomeIcon)
    Vue.component('Layout', DefaultLayout)
}
