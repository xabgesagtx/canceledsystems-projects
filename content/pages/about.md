---
title: About
description: This page gives an overview of the projects created by me
---
This page gives an overview of the projects created by me. This includes only private projects and nothing professional. Of course, this is just a selection. Some outdated or very experimental things are not included.

You can find me on [github](https://github.com/xabgesagtx) or [gitlab](https://gitlab.com/xabgesagtx)