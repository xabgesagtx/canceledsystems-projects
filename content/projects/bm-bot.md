---
title: Bewegungsmelder Bot
yearCreated: 2016
description: Small telegram bot for bewegungsmelder.org website
projectUrl: https://t.me/bm20bot
repositoryUrl: https://gitlab.com/xabgesagtx/bmbot
tags:
  - spring-boot
  - java
  - jvm
  - maven
  - mongodb
  - telegram
---

Minimal bot for accessing the events from the website [bewegungsmelder.org](https://bewegungsmelder.org) via telegram. No need to install an app or open your browser

# Features:
* get events for specific dates
* custom keyboard for improved user experience
* share events via inline queries