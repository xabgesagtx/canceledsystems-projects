---
title: Cloud Image Viewer
yearCreated: 2019
screenshot: /images/cloud-images.png
description: Frontend to images stored in a S3 bucket
projectUrl: https://cloud-images.canceledsystems.com
repositoryUrl: https://gitlab.com/xabgesagtx/cloud-image-viewer
tags:
  - spring-boot
  - kotlin
  - jvm
  - gradle
  - s3
  - thymeleaf
  - webjars
  - bootstrap
  - oauth2
---
### Features
* view images from s3 in browser 
* gallery view of images
* Oauth2 authentication
* batch creation of thumbnails