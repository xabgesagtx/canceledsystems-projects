---
title: Unwetter
yearCreated: 2019
screenshot: /images/unwetter.png
description: Simple weather site using OpenWeatherMap and public alerts
projectUrl: https://unwetter.canceledsystems.com
repositoryUrl: https://gitlab.com/xabgesagtx/unwetter
tags:
  - typescript
  - angular
  - bootstrap
  - leaflet
  - openweathermap
  - npm
---
This mobile ready weather application provides a view on free weather data from OpenWeatherMap. Data from the API is provided via a nginx based proxy. The API key is appended via rewrite rules and is therefore hidden from the user.

To get weather data the app requires you sharing the location. The location is stored in the local storage of your browser and is only used for calls to get the weather data based on coordinates.

Additional for German users weather alerts from NDR are shown.