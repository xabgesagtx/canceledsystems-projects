---
title: Mensa Hamburg API
yearCreated: 2017
screenshot: /images/mensa-api.png
description: API (+website +bot) for mensa menus in Hamburg
projectUrl: https://mensa.canceledsystems.com/
repositoryUrl: https://gitlab.com/xabgesagtx/mensa-api
tags:
  - spring-boot
  - java
  - jvm
  - gradle
  - bootstrap
  - thymeleaf
  - leaflet
  - webjars
  - telegram
  - graphql
  - s3
  - swagger
  - mongodb
---
This project aims to provide the means to create an API for all data about mensas in Hamburg.

### Features
* indexing of all menus for the mensas in Hamburg
* REST-API with Swagger documentation and Swagger UI view
* responsive HTML view of menus
* GraphQL-API with GraphiQL view
* CSV export of mensa data to S3 for usage in other languages and tools (e.g. R)
* access of menus via Telegram bot
* find the mensa closest to you in HTML view and bot