---
title: Movienights
yearCreated: 2019
screenshot: /images/movienights.png
description: Website to coordinate movienights with your friends
projectUrl: https://movienights.canceledsystems.com
repositoryUrl: https://gitlab.com/xabgesagtx/movienights
tags:
  - spring-boot
  - kotlin
  - jvm
  - gradle
  - mongodb
  - typescript
  - angular
  - bootstrap
  - oauth2
---

### Features:
* search movies in the themoviedb
* import them to a library
* upvote/downvote movies
* comment on movie choices