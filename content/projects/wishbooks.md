---
title: Wishbooks
yearCreated: 2019
screenshot: /images/screenshot.png
description: Website to keep track of books you want to have
projectUrl: https://wishbooks.canceledsystems.com
repositoryUrl: https://gitlab.com/xabgesagtx/wishbooks
tags:
  - spring-boot
  - kotlin
  - jvm
  - mongodb
  - gradle
  - typescript
  - angular
  - bootstrap
  - oauth2
---
A simple website to keep track of the books you want. Every user has it's library of books to keep track of. Additionally, you can organize the books in lists (e.g. create a public wishlist).

To make it a bit easier to add books, the [Google Books API](https://developers.google.com/books) is used for searching books before adding them.

### Features

* oauth2 based authentication
* import books from google books API
* edit book details after import
* mark books as wanted, bought or read
* create lists (public and private)
* share public lists