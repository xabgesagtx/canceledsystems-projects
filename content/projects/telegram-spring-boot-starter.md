---
title: Telegram Spring Boot Starter
yearCreated: 2016
description: A spring boot starter for telegram bots
projectUrl: ""
repositoryUrl: https://github.com/xabgesagtx/telegram-spring-boot-starter
tags:
  - spring-boot
  - java
  - jvm
  - maven
  - telegram
---
A small spring boot starter to get started with the [TelegramBot Java API](https://github.com/rubenlagus/TelegramBots).

### Features
* properties for configuring webhook
* library available at maven central