---
title: Fatlining
yearCreated: 2020
screenshot: /images/fat-lining.png
description: Website to keep track of your weight
projectUrl: https://fat-lining.canceledsystems.com/
repositoryUrl: https://gitlab.com/xabgesagtx/fatlining-firebase
tags:
  - typescript
  - angular
  - firebase
  - angular-material
  - npm
  - firestore
---
Website to keep track of weight

### Features
* add weight measurments (and delete if want)
* display graph with trend line
* authentication with google
* data is isolated per user

### Technology
This is a firebase project using firestore with the firebase sdk in the client. This means there is no server or function powering this website. The data is stored in firestore and consistency and isolation are enforced by firestore rules.