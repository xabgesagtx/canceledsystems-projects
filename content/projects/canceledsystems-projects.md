---
title: Canceledsystems Projects
yearCreated: 2020
screenshot: ""
description: Website to display a list of projects (i.e. this website)
projectUrl: https://projects.canceledsystems.com
repositoryUrl: https://gitlab.com/xabgesagtx/canceledsystems-projects
tags:
  - javascript
  - gridsome
  - vue
  - yarn
  - netlify-cms
  - git-lfs
  - graphql
---
List of projects with description and tags

### Technology:
* completely static website based on gridsome
* the git based netlify cms for uploading and editing content
* netlify large media (via git-lfs) for storing images
* netlify identity for authenticating users of the CMS
* bootstrap with bootswatch themes for layout 

### Features:
* detailed view of projects
* list projects by tag
