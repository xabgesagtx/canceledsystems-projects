---
title: TMDB Images
yearCreated: 2019
description: HTTP based image proxy for themoviedb
projectUrl: https://tmdb-images.canceledsystems.com/swagger-ui.html
repositoryUrl: https://gitlab.com/xabgesagtx/tmdb-images
tags:
  - spring-boot
  - kotlin
  - jvm
  - s3
  - gradle
  - open-api
---
An image proxy for themoviedb.org (tmdb)

It uses:
* spring boot for application bootstrapping
* java tmdb api for connecting to tmdb
* S3 for caching images
* amazon SDK to connect to S3