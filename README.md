# Projects on canceledsystems.com

This is the project powering the website projects.canceledsystems.com.

It contains of an overview of all projects that I have created and I think are still relevant.
* completely static website based on gridsome
* the git based netlify cms for uploading and editing content
* netlify large media (via git-lfs) for storing images
* netlify identity for authenticating users of the CMS
* bootstrap with bootswatch themes for layout 

Content types in the CMS are:
* projects: containing a description of the project
* pages: like the about page

Additionally, another type tag is created from the projects which makes filtering per tag possible.

## Development

### 1. Install Gridsome CLI tool if you don't have

`npm install --global @gridsome/cli`

### 2. Run development mode

`gridsome develop` to start a local dev server at `http://localhost:8080`
